FROM php:7-fpm-alpine
RUN apk add --no-cache tzdata
ENV TZ America/Los_Angeles
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
COPY custom-config.ini /usr/local/etc/php/conf.d
RUN apk upgrade --update && apk add \
        freetype \
        libpng \
        libjpeg-turbo \
        freetype-dev \
        libjpeg-turbo-dev \
        libpng-dev \
        oniguruma-dev \
    && docker-php-ext-install iconv pdo_mysql mbstring \
    && docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ \
    && docker-php-ext-install gd mysqli 
RUN sed -i -e "s/listen = \[::\]:9000/listen = 0\.0\.0\.0:9000/g" /usr/local/etc/php-fpm.d/zz-docker.conf
